﻿using UnityEngine;

public class StartPlatform : Platform
{
    [SerializeField] private Ball _ball;
    [SerializeField] private Transform _startPosition;

    private void Awake()
    {
        Instantiate(_ball, _startPosition.position, Quaternion.identity);
    }
}
