﻿using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField] private float _explodeForce;
    [SerializeField] private float _explodeRadius;

    public void Break()
    {
        var platformSegments = GetComponentsInChildren<PlatformSegment>();

        foreach (PlatformSegment segment in platformSegments)
        {
            Debug.Log(segment.name);
            segment.Explode(_explodeForce, transform.position, _explodeRadius);
        }
    }
}
