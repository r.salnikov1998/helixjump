﻿using UnityEngine;
using Random = UnityEngine.Random;

public class TowerBuilder : MonoBehaviour
{
    [SerializeField] private int _amountOfPlatforms;
    [SerializeField] private float _extraScale;
    [SerializeField] private GameObject _beam;
    [SerializeField] private GameObject _towerContainer;
    [SerializeField] private StartPlatform _startPlatform;
    [SerializeField] private FinishPlatform _finishPlatform;
    [SerializeField] private Platform[] _platforms;

    private float _startAndFinishExtraScale = 0.5f;

    private float BeamScaleY => _amountOfPlatforms / 2f + _startAndFinishExtraScale + _extraScale / 2;

    private void Awake()
    {
        Build();
    }

    private void Build()
    {
        GameObject beam = Instantiate(_beam, _towerContainer.transform);
        beam.transform.localScale = new Vector3(1, BeamScaleY, 1);

        Vector3 spawnPosition = beam.transform.position;
        spawnPosition.y += beam.transform.localScale.y - _extraScale;

        SpawnPlatform(_startPlatform, ref spawnPosition);

        for (int i = 0; i < _amountOfPlatforms; i++)
        {
            SpawnPlatform(_platforms[Random.Range(0, _platforms.Length)], ref spawnPosition);
        }
        
        SpawnPlatform(_finishPlatform, ref spawnPosition);

    }
    
    private void SpawnPlatform(Platform platform, ref Vector3 spawnPosition)
    {
        var spawned = Instantiate(platform, spawnPosition, Quaternion.Euler(0, Random.Range(0, 180), 0), _towerContainer.transform);
        var localScale = spawned.transform.localScale;
        localScale = new Vector3(localScale.x, localScale.y, 1);
        spawned.transform.localScale = localScale;
        spawnPosition.y -= 1;
    }
}
