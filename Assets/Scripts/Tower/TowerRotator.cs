﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TowerRotator : MonoBehaviour
{
    [SerializeField] private float _rotateSpeed;

    private Rigidbody _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        // _rigidbody.AddTorque(Vector3.up * Time.deltaTime * _rotateSpeed);
        // Vector3 resultEuler = transform.eulerAngles + Vector3.up * _rotateSpeed * (-1);
        // _rigidbody.MoveRotation(Quaternion.Euler(resultEuler));

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
                // float torque = touch.deltaPosition.x * Time.deltaTime * _rotateSpeed;
                // _rigidbody.AddTorque(Vector3.up * torque);
                Vector3 deltaEuler = Vector3.up * _rotateSpeed * touch.deltaPosition.x * (-1);
                Vector3 resultEuler = transform.eulerAngles + deltaEuler;
                _rigidbody.MoveRotation(Quaternion.Euler(resultEuler));
            }
        }
    }
}