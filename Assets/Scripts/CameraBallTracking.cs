﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBallTracking : MonoBehaviour
{
    [SerializeField] private Vector3 _directionOffset;
    [SerializeField] private float _length;

    private Ball _ball;
    private Beam _beam;
    private Vector3 _cameraPosition;
    private Vector3 _minimalBallPosition;

    private void Start()
    {
        _ball = FindObjectOfType<Ball>();
        _beam = FindObjectOfType<Beam>();

        Vector3 ballPosition = _ball.transform.position;
        _cameraPosition = ballPosition;
        _minimalBallPosition = ballPosition;
    }


    private void Update()
    {
        if (_ball.transform.position.y < _minimalBallPosition.y)
        {
            TrackBall();
            _minimalBallPosition = _ball.transform.position;
        }
    }

    private void TrackBall()
    {
        Vector3 beamPosition = _beam.transform.position;
        Vector3 ballPosition = _ball.transform.position;
        beamPosition.y = ballPosition.y;
        _cameraPosition = ballPosition;
        Vector3 direction = (beamPosition - ballPosition).normalized + _directionOffset;
        _cameraPosition -= direction * _length;
        transform.position = _cameraPosition;
        transform.LookAt(_ball.transform);
    }
}